<?php

/**
 * Class PDO Connection
 */
class PDOConnection
{

    private function __construct()
    {
    }

    public static function getConnection()
    {
        $host = 'localhost';
        $port = '3306';
        $user = 'root';
        $pass = '';
        $name = 'optica-test';

        $dsn = "mysql:host=$host;port=$port;dbname=$name";

        try {
            $connection = new PDO($dsn, $user, $pass);
            return $connection;
        } catch (PDOException $e) {
            die($e);
        }
    }
}
