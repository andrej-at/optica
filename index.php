<?php

spl_autoload_register(function ($class_name) {
    $directorys = array(
        'lib/'
    );

    foreach($directorys as $directory)
    {
        if(file_exists($directory.$class_name . '.php'))
        {
            require_once($directory.$class_name . '.php');
            return;
        }
    }

});

class Core{
    public static $_instaAcc = "";
    public static $_postsCount = 0;

    public static function handle(){
        $_instaAcc = $_SERVER['argv'][1];
        $_postsCount = $_SERVER['argv'][2];

        $signedUsers = self::checkUsers();

        $posts = self::getPosts($_postsCount);

        foreach ($posts as $postId => $post){
            $activitySubsUsers = array_intersect_assoc($signedUsers, $post);
            $countActivitySubsUsers = count($activitySubsUsers);
            $countActivityUnsignUsers = count($post) - $countActivitySubsUsers;
            self::insertPost($postId, time(),
                $countActivitySubsUsers, $countActivityUnsignUsers);
        }
    }

    public static function checkUsers(){
        $userList = self::getUsersList();
        if (!$userList){
            $signedUsers = self::getUsers(1);
            foreach($signedUsers as $username=>$fullname){
                self::insertUser($username, $fullname, 1);
            }
        }else{
            $newUsers = self::getUsers(0);

            $newSignedUsers = array_diff_assoc($newUsers, $userList);
            $newUnsignedUsers = array_diff_assoc($userList, $newUsers);

            foreach($newSignedUsers as $username => $fullname){
                self::insertUser($username, $fullname, 1);
            }

            foreach($newUnsignedUsers as $username => $fullname){
                self::updateUser($username, $fullname, 0);
            }
        }

        return self::getUsersList();
    }

    public static function getUsers($first){
        if ($first){
            $users = array(
                "permanent_faceup" => "Перманентный макияж и уход",
                "lanasukhaya" => "О лице и теле с Душой",
                "f.community" => "Тренировки Путешествия",
                "kuhninazakaz.kz" => "Кухни На Заказ Алматы",
                "totem.pyatigorsk" => "Totem.pyatigorsk",
                "zarinauzenalieva" => "blabla",
                "svadebnie_platia_optom_" => "Свадебный Салон В Атырау",
                "55_erer" => "ErEr_55",
                "paverbih228" => "Илья Прокопьев",
                "_j4y_s2hi_" => "blabla",
                "flariby" => "Anatoli Sukhaverau",
                "gulmirahanum9268" => "Cerikkyzy Gulmirahanum",
                "viramax_ast" => "ВИРАМАКС АСТАНА",
                "biznes_v_instagram" => "Реклама в Инстаграм",
                "zulfiashaikhimova" => "Зульфя Шайхимова",
                "amangeldieva.zhazira" => "Aloe_aman",
                "djokker8" => "Баха-Баги",
                "artur.don" => "РАСКРУТКА ИНСТАГРАМ",
                "nikita.es.21" => "Nikita Lutsenko",
                "aktivnosti.gruppa" => "LIKETIME CHAT",
                "__kyzzhibek__" => "ПРОКАТ ПЛАТЬЕВ",
                "zikenov3993" => "Нурбол",
                "shedite_naadan" => "Шэдитэ наадан",
                "portret67" => "Рисую ПОРТРЕТЫ на заказ"
            );
        }else{
            $users = array(
                "permanent_faceup" => "Перманентный макияж и уход",
                "lanasukhaya" => "О лице и теле с Душой",
                "f.community" => "Тренировки Путешествия",
                "kuhninazakaz.kz" => "Кухни На Заказ Алматы",
                "totem.pyatigorsk" => "Totem.pyatigorsk",
                "zarinauzenalieva" => "blabla",
                "svadebnie_platia_optom_" => "Свадебный Салон В Атырау",
                "55_erer" => "ErEr_55",
                "paverbih228" => "Илья Прокопьев",
                "_j4y_s2hi_" => "blabla",
                "flariby" => "Anatoli Sukhaverau",
                "gulmirahanum9268" => "Cerikkyzy Gulmirahanum",
                "tratata" => "tratata 1",
                "tratata2" => "tratata 2",
                "tratata3" => "tratata 3",
            );
        }
        return $users;
    }

    public static function getUsersList(){
        $conn = PDOConnection::getConnection();

        try {
            $sql = "SELECT username, fullname FROM `users` WHERE 1";
            $stmt = $conn->prepare($sql);

            $stmt->execute();
            $query = $stmt->fetchAll();

            if ($query) {
                $result = array();
                foreach ($query as $key => $value) {
                    $result[$value['username']] = $value['fullname'];
                }
                return $result;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            echo "DataBase Error.<br/>" . $e->getMessage();
        } catch (Exception $e) {
            echo "General Error.<br/>" . $e->getMessage();
        }
        finally {
            $conn = null;
        }
    }

    public static function insertUser($username, $fullname, $signed){
        $conn = PDOConnection::getConnection();

        try {
            $sql = "INSERT IGNORE INTO `users` (username, fullname, signed)
                values(:username, :fullname, :signed)";
            $stmt = $conn->prepare($sql);
            $stmt->bindValue(":username", $username, PDO::PARAM_STR);
            $stmt->bindValue(":fullname", $fullname, PDO::PARAM_STR);
            $stmt->bindValue(":signed", (int)$signed, PDO::PARAM_INT);

            $stmt->execute();
            $query = $stmt->fetchObject();

            if ($query) {
                return true;
            } else {
                return false;
            }

        } catch (PDOException $e) {
            echo "DataBase Error.<br/>" . $e->getMessage();
        } catch (Exception $e) {
            echo "General Error.<br/>" . $e->getMessage();
        }
        finally {
            $conn = null;
        }
    }

    public static function updateUser($username, $fullname, $signed){
        $conn = PDOConnection::getConnection();

        try {
            $sql = "UPDATE `users` SET `signed` = :signed WHERE
                username = :username AND fullname = :fullname";
            $stmt = $conn->prepare($sql);
            $stmt->bindValue(":signed", (int)$signed);
            $stmt->bindValue(":username", $username);
            $stmt->bindValue(":fullname", $fullname);

            $stmt->execute();
            $query = $stmt->fetchObject();

            if ($query) {
                return true;
            } else {
                return false;
            }

        } catch (PDOException $e) {
            echo "DataBase Error.<br/>" . $e->getMessage();
        } catch (Exception $e) {
            echo "General Error.<br/>" . $e->getMessage();
        }
        finally {
            $conn = null;
        }
    }

    public static function getPosts($count){
        $posts = array(
            "a1" => array(
                "tratata" => "tratata 1",
                "tratata2" => "tratata 2",
                "tratata3" => "tratata 3",
                "zzzzzzzzz" => "yyyyyyyyy",
                "zzzzzzzzz1" => "yyyyyyyyy1",
                "zzzzzzzzz2" => "yyyyyyyyy2"
            ),
            "a2" => array(
                "shedite_naadan" => "Шэдитэ наадан",
                "tratata" => "tratata 1",
                "tratata2" => "tratata 2",
                "tratata3" => "tratata 3",
                "zzzzzzzzz" => "yyyyyyyyy",
                "zzzzzzzzz1" => "yyyyyyyyy1",
            ),
            "a3" => array(
                "totem.pyatigorsk" => "Totem.pyatigorsk",
                "shedite_naadan" => "Шэдитэ наадан",
                "tratata" => "tratata 1",
                "zzzzzzzzz1" => "yyyyyyyyy1",
                "zzzzzzzzz2" => "yyyyyyyyy2"
            ),
            "a4" => array(
                "viramax_ast" => "ВИРАМАКС АСТАНА",
                "biznes_v_instagram" => "Реклама в Инстаграм",
                "zulfiashaikhimova" => "Зульфя Шайхимова",
                "amangeldieva.zhazira" => "Aloe_aman",
                "zzzzzzzzz2" => "yyyyyyyyy2"
            ),
            "a5" => array(
                "artur.don" => "РАСКРУТКА ИНСТАГРАМ",
                "nikita.es.21" => "Nikita Lutsenko",
                "aktivnosti.gruppa" => "LIKETIME CHAT",
                "zzzzzzzzz2" => "yyyyyyyyy2"
            ),
            "a6" => array("viramax_ast" => "ВИРАМАКС АСТАНА",
                "biznes_v_instagram" => "Реклама в Инстаграм",
                "zulfiashaikhimova" => "Зульфя Шайхимова",
                "amangeldieva.zhazira" => "Aloe_aman",
                "tratata2" => "tratata 2",
                "tratata3" => "tratata 3",
                "zzzzzzzzz2" => "yyyyyyyyy2"
            ),
            "a7" => array("viramax_ast" => "ВИРАМАКС АСТАНА",
                "zulfiashaikhimova" => "Зульфя Шайхимова",
                "amangeldieva.zhazira" => "Aloe_aman",
                "tratata2" => "tratata 2",
                "zzzzzzzzz2" => "yyyyyyyyy2"
            ),
            "a8" => array("viramax_ast" => "ВИРАМАКС АСТАНА",
                "biznes_v_instagram" => "Реклама в Инстаграм",
                "zzzzzzzzz2" => "yyyyyyyyy2"
            ),
            "a9" => array("viramax_ast" => "ВИРАМАКС АСТАНА",
                "biznes_v_instagram" => "Реклама в Инстаграм",
                "zulfiashaikhimova" => "Зульфя Шайхимова",
                "amangeldieva.zhazira" => "Aloe_aman",
                "zzzzzzzzz2" => "yyyyyyyyy2"
            ),
            "a10" => array("viramax_ast" => "ВИРАМАКС АСТАНА",
                "biznes_v_instagram" => "Реклама в Инстаграм",
                "zulfiashaikhimova" => "Зульфя Шайхимова",
                "amangeldieva.zhazira" => "Aloe_aman",
                "zzzzzzzzz2" => "yyyyyyyyy2"
            ),
            "a11" => array("viramax_ast" => "ВИРАМАКС АСТАНА",
                "biznes_v_instagram" => "Реклама в Инстаграм",
                "zulfiashaikhimova" => "Зульфя Шайхимова",
                "amangeldieva.zhazira" => "Aloe_aman",
                "zzzzzzzzz2" => "yyyyyyyyy2"
            ),
            "a12" => array("viramax_ast" => "ВИРАМАКС АСТАНА",
                "biznes_v_instagram" => "Реклама в Инстаграм",
                "zulfiashaikhimova" => "Зульфя Шайхимова",
                "amangeldieva.zhazira" => "Aloe_aman",
                "zzzzzzzzz2" => "yyyyyyyyy2"
            ),
            "a13" => array("viramax_ast" => "ВИРАМАКС АСТАНА",
                "biznes_v_instagram" => "Реклама в Инстаграм",
                "zulfiashaikhimova" => "Зульфя Шайхимова",
                "amangeldieva.zhazira" => "Aloe_aman",
                "zzzzzzzzz2" => "yyyyyyyyy2"
            ),
            "a14" => array("viramax_ast" => "ВИРАМАКС АСТАНА",
                "biznes_v_instagram" => "Реклама в Инстаграм",
                "zulfiashaikhimova" => "Зульфя Шайхимова",
                "amangeldieva.zhazira" => "Aloe_aman",
                "zzzzzzzzz2" => "yyyyyyyyy2"
            ),
            "a15" => array("viramax_ast" => "ВИРАМАКС АСТАНА",
                "biznes_v_instagram" => "Реклама в Инстаграм",
                "zulfiashaikhimova" => "Зульфя Шайхимова",
                "amangeldieva.zhazira" => "Aloe_aman",
                "zzzzzzzzz2" => "yyyyyyyyy2"
            ),
            "a16" => array(
                "tratata" => "tratata 1",
                "tratata2" => "tratata 2",
                "tratata3" => "tratata 3",
                "zzzzzzzzz2" => "yyyyyyyyy2"
            ),
            "a17" => array(
                "shedite_naadan" => "Шэдитэ наадан",
                "tratata" => "tratata 1",
                "tratata2" => "tratata 2",
                "tratata3" => "tratata 3",
                "zzzzzzzzz2" => "yyyyyyyyy2"
            ),
            "a18" => array(
                "totem.pyatigorsk" => "Totem.pyatigorsk",
                "shedite_naadan" => "Шэдитэ наадан",
                "tratata" => "tratata 1",
                "zzzzzzzzz2" => "yyyyyyyyy2"
            ),
            "a19" => array(
                "viramax_ast" => "ВИРАМАКС АСТАНА",
                "biznes_v_instagram" => "Реклама в Инстаграм",
                "zulfiashaikhimova" => "Зульфя Шайхимова",
                "amangeldieva.zhazira" => "Aloe_aman",
                "zzzzzzzzz2" => "yyyyyyyyy2"
            ),
            "a20" => array(
                "artur.don" => "РАСКРУТКА ИНСТАГРАМ",
                "nikita.es.21" => "Nikita Lutsenko",
                "aktivnosti.gruppa" => "LIKETIME CHAT",
                "zzzzzzzzz2" => "yyyyyyyyy2"
            ),
            "a21" => array("viramax_ast" => "ВИРАМАКС АСТАНА",
                "biznes_v_instagram" => "Реклама в Инстаграм",
                "zulfiashaikhimova" => "Зульфя Шайхимова",
                "amangeldieva.zhazira" => "Aloe_aman",
                "tratata2" => "tratata 2",
                "tratata3" => "tratata 3",
                "zzzzzzzzz2" => "yyyyyyyyy2"
            ),
            "a22" => array("viramax_ast" => "ВИРАМАКС АСТАНА",
                "zulfiashaikhimova" => "Зульфя Шайхимова",
                "amangeldieva.zhazira" => "Aloe_aman",
                "tratata2" => "tratata 2",
                "zzzzzzzzz2" => "yyyyyyyyy2"
            ),
            "a23" => array("viramax_ast" => "ВИРАМАКС АСТАНА",
                "biznes_v_instagram" => "Реклама в Инстаграм",
            ),
            "a24" => array("viramax_ast" => "ВИРАМАКС АСТАНА",
                "biznes_v_instagram" => "Реклама в Инстаграм",
                "zulfiashaikhimova" => "Зульфя Шайхимова",
                "amangeldieva.zhazira" => "Aloe_aman"
            ),
            "a25" => array("viramax_ast" => "ВИРАМАКС АСТАНА",
                "biznes_v_instagram" => "Реклама в Инстаграм",
                "zulfiashaikhimova" => "Зульфя Шайхимова",
                "amangeldieva.zhazira" => "Aloe_aman"
            ),
            "a26" => array("viramax_ast" => "ВИРАМАКС АСТАНА",
                "biznes_v_instagram" => "Реклама в Инстаграм",
                "zulfiashaikhimova" => "Зульфя Шайхимова",
                "amangeldieva.zhazira" => "Aloe_aman"
            ),
            "a27" => array("viramax_ast" => "ВИРАМАКС АСТАНА",
                "biznes_v_instagram" => "Реклама в Инстаграм",
                "zulfiashaikhimova" => "Зульфя Шайхимова",
                "amangeldieva.zhazira" => "Aloe_aman"
            ),
            "a28" => array("viramax_ast" => "ВИРАМАКС АСТАНА",
                "biznes_v_instagram" => "Реклама в Инстаграм",
                "zulfiashaikhimova" => "Зульфя Шайхимова",
                "amangeldieva.zhazira" => "Aloe_aman"
            ),
            "a29" => array("viramax_ast" => "ВИРАМАКС АСТАНА",
                "biznes_v_instagram" => "Реклама в Инстаграм",
                "zulfiashaikhimova" => "Зульфя Шайхимова",
                "amangeldieva.zhazira" => "Aloe_aman",
                "zzzzzzzzz2" => "yyyyyyyyy2"
            ),
            "a30" => array("viramax_ast" => "ВИРАМАКС АСТАНА",
                "biznes_v_instagram" => "Реклама в Инстаграм",
                "zulfiashaikhimova" => "Зульфя Шайхимова",
                "amangeldieva.zhazira" => "Aloe_aman",
                "zzzzzzzzz2" => "yyyyyyyyy2"
            )
        );
        return array_slice($posts, 0, $count);
    }

    public static function insertPost($postId, $time, $countActivitySubsUsers,
        $countActivityUnsignUsers){

        $conn = PDOConnection::getConnection();

        try {
            $sql = "INSERT IGNORE INTO `posts`
                (post_id, time, activity_subs_users, activity_unsig_users)
                values(
                    :post_id, :time, :activity_subs_users, :activity_unsig_users
                );";

            $stmt = $conn->prepare($sql);
            $stmt->bindValue(":post_id", $postId, PDO::PARAM_STR);
            $stmt->bindValue(":time", (int)$time, PDO::PARAM_INT);
            $stmt->bindValue(":activity_subs_users",
                (int)$countActivitySubsUsers, PDO::PARAM_INT);
            $stmt->bindValue(":activity_unsig_users",
                (int)$countActivityUnsignUsers, PDO::PARAM_INT);

            $stmt->execute();
            $query = $stmt->fetchObject();

            if ($query) {
                return true;
            } else {
                return false;
            }

        } catch (PDOException $e) {
            echo "DataBase Error.<br/>" . $e->getMessage();
        } catch (Exception $e) {
            echo "General Error.<br/>" . $e->getMessage();
        }
        finally {
            $conn = null;
        }
    }
}

Core::handle();
